from django.test import TestCase
from django.contrib.auth import get_user_model

User = get_user_model()

# Create your tests here.
class UserTestCase(TestCase) :
    def setUp(self):
        User.objects.create(username="admin")
        pass

    def test_admin_user(self) :
        user = User.objects.filter(username='admin').first()
        print(user)
        self.assertIsNotNone(user)